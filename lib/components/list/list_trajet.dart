import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

var title = [
  "Head southweston Madison St",
  "Turn left onto  4th Ave",
  "Turn right at 105th N Link Rd",
  "Turn right at 105 William St, Chicago, US",
  "Continue straight to stay on Vancouver",
  "Keep left , follow signs for SF Intl Airport",
];

var sousTitle = [
  "18 miles",
  "12 miles",
  "40 miles",
  "250 miles",
  "24 miles",
  "",
];

var details = [
  "",
  "",
  "Pass by Executive Hotel Pacific (on the left)",
  "",
  "Entering California",
  "",
];

List<Icon> listIcon = [
  Icon(Icons.north),
  Icon(AntDesign.back),
  Icon(Feather.corner_up_right),
  Icon(Feather.corner_up_right),
  Icon(Icons.north),
  Icon(AntDesign.back),
];

var divider = Container(
  padding: EdgeInsets.symmetric(horizontal: 20),
  child: Divider(
    height: 1,
    color: Colors.grey[500],
  ),
);
