import 'package:flutter/material.dart';
import 'package:home_online/constantes/constants.dart';

class HeaderContainer extends StatelessWidget {

  HeaderContainer({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.subTitle,

  }) : super(key: key);


  final String title;
  final String subTitle;
  Icon icon;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10, left: 10),
      color: mbSeconderedColorCpfind,
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          icon,
          SizedBox(width: 10),
          Text(subTitle, style: TextStyle(color: Colors.grey[900], fontSize: 15)),
          SizedBox(width: 5),
          Text(title, style: TextStyle(color: Colors.grey[900], fontSize: 15)),
        ],
      ),
    );
  }

}