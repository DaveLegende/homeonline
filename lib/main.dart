import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:home_online/components/header_container.dart';
import 'package:home_online/components/list/list_trajet.dart';
import 'package:home_online/constantes/constants.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Home Online'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<bool> isClicked = [false, false, false, true, false, false];

  final ScrollController controller = ScrollController();

  Widget container = HeaderContainer(
    icon: listIcon[3],
    subTitle: "250 miles",
    title: "Turn right at 105 William St, Chicago, US",
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Pick up", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: mbSeconderedColorCpfind),
            onPressed: null),
      ),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            child: container,
          ),
          SlidingUpPanel(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            minHeight: MediaQuery.of(context).size.height * 0.25,
            maxHeight: MediaQuery.of(context).size.height * 0.78,
            body: Container(
              color: Colors.transparent,
            ),
            panelBuilder: (controller) {
              return _slidingUpPanelBody(controller);
            },
          ),
          // Container(
          //   padding: EdgeInsets.only(top: 50),
          //   child: DraggableScrollableSheet(builder: (context, controller) {
          //     return Container(
          //       padding: EdgeInsets.only(top: 5),
          //       decoration: BoxDecoration(
          //         color: Colors.white,
          //         borderRadius: BorderRadius.only(
          //           topLeft: Radius.circular(20),
          //           topRight: Radius.circular(20),
          //         ),
          //       ),
          //       child: Column(children: [
          //         Center(
          //           child: Container(
          //             width: 50,
          //             height: 5,
          //             decoration: BoxDecoration(
          //               color: Colors.grey[300],
          //               borderRadius: BorderRadius.circular(2),
          //             ),
          //           ),
          //         ),
          //         paddingTopBottom(),
          //         Container(
          //           child: Row(
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.only(left: 20),
          //                 child: InkWell(
          //                   onTap: () {},
          //                   child: ClipOval(
          //                     child: Image(
          //                       width: 50,
          //                       height: 50,
          //                       image: AssetImage("assets/raph.jpg"),
          //                       fit: BoxFit.cover,
          //                     ),
          //                   ),
          //                 ),
          //               ),
          //               paddingRightLeft(),
          //               Column(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 crossAxisAlignment: CrossAxisAlignment.start,
          //                 children: [
          //                   Text("Pick up at",
          //                       style: TextStyle(color: Colors.grey[400])),
          //                   Text("7958 Swift Village",
          //                       style: TextStyle(
          //                         color: Colors.black,
          //                         fontSize: 20.0,
          //                         fontFamily: "Time News Romans",
          //                       )),
          //                 ],
          //               ),
          //             ],
          //           ),
          //         ),
          //         paddingTopBottom(),
          //         Container(
          //           padding: EdgeInsets.symmetric(horizontal: 10),
          //           child: Divider(
          //             height: 1,
          //             color: Colors.grey[500],
          //           ),
          //         ),
          //         paddingTopBottom(),
          //         headDraggableSheet(context),
          //         Container(
          //           padding: EdgeInsets.symmetric(horizontal: 10),
          //           child: Divider(
          //             height: 1,
          //             color: Colors.grey[500],
          //           ),
          //         ),
          //         listViewCourse(),
          //       ]),
          //     );
          //   }),
          // ),
        ],
      ),
    );
  }

  Widget listViewCourse() {
    return Container(
      child: ListView.builder(
          controller: this.controller,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: title.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                setState(() {
                  isClicked[index] = true;
                  container = HeaderContainer(
                    icon: listIcon[index],
                    subTitle: sousTitle[index],
                    title: title[index],
                  );
                });
              },
              child: Column(children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.only(left: 10)),
                      IconButton(
                        icon: listIcon[index],
                        onPressed: null,
                        color: isClicked[index]
                                ? mbSeconderedColorCpfind
                                : Colors.grey,
                      ),
                      Expanded(
                        child: Text(
                          "${title[index]}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: isClicked[index]
                                ? mbSeconderedColorCpfind
                                : Colors.grey[900],
                            fontSize: 20.0,
                            fontFamily: "Time News Romans",
                          ),
                        ),
                      ),
                      paddingBottom(),
                    ],
                  ),
                ),
                Text("${details[index]}",
                    style: TextStyle(
                      color: isClicked[index]
                          ? mbSeconderedColorCpfind
                          : Colors.black,
                      fontSize: 10,
                    )),
                paddingBottom(),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.only(left: 58),
                          child: Text("${sousTitle[index]}",
                              style: TextStyle(
                                color: isClicked[index]
                                    ? mbSeconderedColorCpfind
                                    : Colors.black,
                              ))),
                      flex: 1,
                    ),
                    Expanded(
                      flex: 2,
                      child: divider,
                    ),
                  ],
                ),
                paddingBottom(),
              ]),
            );
          }),
    );
  }

  Container headDraggableSheet(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("Est", style: TextStyle(color: Colors.grey[400])),
              Text("Distance", style: TextStyle(color: Colors.grey[400])),
              Text("Fare", style: TextStyle(color: Colors.grey[400])),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("5 min",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontFamily: "Time News Romans",
                  )),
              Text("2.2km",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontFamily: "Time News Romans",
                  )),
              Text("\$25.00",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontFamily: "Time News Romans",
                  )),
            ],
          ),
          Container(
            padding: EdgeInsets.only(right: 20.0, left: 20.0),
            height: MediaQuery.of(context).size.width * 0.2,
            child: Center(
              child: RaisedButton(
                color: appBarColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "DROP OFF",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontFamily: "Time News Romans",
                      ),
                    )
                  ],
                ),
                onPressed: () {},
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding paddingBottom() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
    );
  }

  Padding paddingTopBottom() {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
    );
  }

  Padding paddingRightLeft() {
    return Padding(
      padding: EdgeInsets.only(right: 10, left: 10),
    );
  }

  SingleChildScrollView _slidingUpPanelBody(ScrollController controller) {
    return SingleChildScrollView(
      controller: controller,
      child: Column(
        children: [
          Center(
            child: Container(
              width: 50,
              height: 5,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(2),
              ),
            ),
          ),
          paddingTopBottom(),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 20),
                  child: InkWell(
                    onTap: () {},
                    child: ClipOval(
                      child: Image(
                        width: 50,
                        height: 50,
                        image: AssetImage("assets/raph.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                paddingRightLeft(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Pick up at",
                        style: TextStyle(color: Colors.grey[400])),
                    Text("7958 Swift Village",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontFamily: "Time News Romans",
                        )),
                  ],
                ),
              ],
            ),
          ),
          paddingTopBottom(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Divider(
              height: 1,
              color: Colors.grey[500],
            ),
          ),
          paddingTopBottom(),
          headDraggableSheet(context),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Divider(
              height: 1,
              color: Colors.grey[500],
            ),
          ),
          listViewCourse(),
        ],
      ),
    );
  }
}
